﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
namespace wsServicio
{
    /// <summary>
    /// Descripción breve de svcServicio
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class svcServicio : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        private SqlConnection ObtenerConexion()
        {
            SqlConnection cnnConexion = new SqlConnection("Server=FLIA-FLOREZ; Database=Inventario; Trusted_Connection=True;");
            cnnConexion.Open();
            return cnnConexion;
        }
        [WebMethod]
        public void InsertarPersona(string cedula, string nombre, string apellido, string telefono, string direccion )
        {
            SqlConnection cnnConexion = ObtenerConexion();

            string strSentenciaSQL = "insert into cliente Values ('{0}', '{1}', '{2}', '{3}', '{4}' )";
            strSentenciaSQL = string.Format(strSentenciaSQL, cedula, nombre, apellido, telefono, direccion);
            
            SqlCommand cmdComando = new SqlCommand(strSentenciaSQL, cnnConexion);
            
            cmdComando.ExecuteNonQuery();
            cnnConexion.Close();
        }
        [WebMethod]
        /*
         Verifica si la cedula de un cliente ya existe en la base de datos
         * por ende se tiene como parámetro el número de cédula del cliente 
         * @Autor Katherine Florez
         */
        public DataSet BuscarCliente(string pCedula)
        {

            SqlConnection cnnConexion = ObtenerConexion();

            string strSentenciaSQL = "select * from cliente where cedula='{0}'";
            strSentenciaSQL = string.Format(strSentenciaSQL, pCedula);

            SqlCommand cmdComando = new SqlCommand(strSentenciaSQL, cnnConexion);


            SqlDataAdapter adpAdapter = new SqlDataAdapter(cmdComando);

            DataSet dsConsulta = new DataSet();

            adpAdapter.Fill(dsConsulta, "consulta");

            cnnConexion.Close();

            return dsConsulta;

        }
        public DataSet BuscarProducto(string pTextobusqueda)
        {

            SqlConnection cnnConexion = ObtenerConexion();

            string strSentenciaSQL = "select cod,nombre,cantidad,pvp from producto where nombre like '%'+ '{0}' + '%' ";
            strSentenciaSQL = string.Format(strSentenciaSQL, pTextobusqueda);

            SqlCommand cmdComando = new SqlCommand(strSentenciaSQL, cnnConexion);
           

            SqlDataAdapter adpAdapter = new SqlDataAdapter(cmdComando);

            DataSet dsProducto = new DataSet();

            adpAdapter.Fill(dsProducto,"productos");

            cnnConexion.Close();

            return dsProducto;

        }

         
    
    
        }


    }

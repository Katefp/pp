﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Modelos
{
    public class cliente
    {
        public string nombre; 
        public string apellido;
        public string direccion;
        public string telefono;
        public string cedula;

        //parte de la factura
        public double monto;
        public DateTime fecha;

        wsServicio.svcServicio services= new wsServicio.svcServicio();
        public cliente(){
        }


        public void InsertarEnBaseDatos(cliente pCliente)
        {
            ///Metodo de inserciò0n
           services.InsertarPersona(pCliente.cedula, pCliente.nombre, pCliente.apellido, pCliente.telefono, pCliente.direccion);
           
        }
        public static void insertar(cliente pCliente) {
            cliente clie = new cliente();
            clie.InsertarEnBaseDatos(pCliente);
        }

        
        /* método que retorna si la cédula del cliente ya existe en la base de datos*/
        public cliente BuscarCliente(string pCedula)
        {
            cliente clie = new cliente();

            DataSet dsresultado = services.BuscarCliente(pCedula);

            if (dsresultado.Tables[0].Rows.Count != 0)
            {
            
                clie.cedula = dsresultado.Tables[0].Rows[0]["cedula"].ToString();
                clie.nombre = dsresultado.Tables[0].Rows[0]["nombre"].ToString();
                clie.apellido = dsresultado.Tables[0].Rows[0]["apellido"].ToString();
                clie.direccion = dsresultado.Tables[0].Rows[0]["direccion"].ToString();
                clie.telefono = dsresultado.Tables[0].Rows[0]["telefono"].ToString();

                return clie;
            }
            else
            {
                return clie;
            }
           
                
        }
    }

    
}

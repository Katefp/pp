﻿namespace WindowsFormsApplication2
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbFecha = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Home = new System.Windows.Forms.ToolStripLabel();
            this.Proveedores = new System.Windows.Forms.ToolStripButton();
            this.Producto = new System.Windows.Forms.ToolStripSplitButton();
            this.Reporte = new System.Windows.Forms.ToolStripSplitButton();
            this.Ayuda = new System.Windows.Forms.ToolStripButton();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCedula = new System.Windows.Forms.TextBox();
            this.lbCedula = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.lbDireccion = new System.Windows.Forms.Label();
            this.lbNombre = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.lbTelefono = new System.Windows.Forms.Label();
            this.lbApellido = new System.Windows.Forms.Label();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.lbCliente = new System.Windows.Forms.Label();
            this.txtNombreProducto = new System.Windows.Forms.TextBox();
            this.lbFactura = new System.Windows.Forms.Label();
            this.lbCodBarra = new System.Windows.Forms.Label();
            this.txtCodBarra = new System.Windows.Forms.TextBox();
            this.lbNombreProducto = new System.Windows.Forms.Label();
            this.lbPrecio = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.lbCantidad = new System.Windows.Forms.Label();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.DataInformativo = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Datafacturaventa = new System.Windows.Forms.DataGridView();
            this.Nombre_Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad_Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pvp_Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnFacturar = new System.Windows.Forms.Button();
            this.lbCargaVuelto = new System.Windows.Forms.Label();
            this.txtpago = new System.Windows.Forms.TextBox();
            this.lbVuelto = new System.Windows.Forms.Label();
            this.lbPago = new System.Windows.Forms.Label();
            this.lbSubTotal = new System.Windows.Forms.Label();
            this.lbTotalFactura = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.codDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cantidadDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pvpDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.estadoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.inventarioDataSet1 = new WindowsFormsApplication2.InventarioDataSet1();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.productoTableAdapter = new WindowsFormsApplication2.InventarioDataSet1TableAdapters.productoTableAdapter();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataInformativo)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Datafacturaventa)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventarioDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.lbFecha);
            this.panel1.Location = new System.Drawing.Point(0, 60);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1030, 16);
            this.panel1.TabIndex = 2;
            // 
            // lbFecha
            // 
            this.lbFecha.AutoSize = true;
            this.lbFecha.Location = new System.Drawing.Point(475, 3);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(40, 13);
            this.lbFecha.TabIndex = 0;
            this.lbFecha.Text = "Fecha:";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(45, 50);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Home,
            this.Proveedores,
            this.Producto,
            this.Reporte,
            this.Ayuda});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Margin = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1030, 57);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Home
            // 
            this.Home.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Home.Image = ((System.Drawing.Image)(resources.GetObject("Home.Image")));
            this.Home.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(45, 54);
            this.Home.Text = "Home";
            this.Home.ToolTipText = "Home";
            // 
            // Proveedores
            // 
            this.Proveedores.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Proveedores.DoubleClickEnabled = true;
            this.Proveedores.Image = ((System.Drawing.Image)(resources.GetObject("Proveedores.Image")));
            this.Proveedores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Proveedores.Margin = new System.Windows.Forms.Padding(15, 1, 0, 2);
            this.Proveedores.Name = "Proveedores";
            this.Proveedores.Size = new System.Drawing.Size(49, 54);
            this.Proveedores.Text = "Proveedores";
            this.Proveedores.Click += new System.EventHandler(this.Proveedores_Click);
            // 
            // Producto
            // 
            this.Producto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Producto.Image = ((System.Drawing.Image)(resources.GetObject("Producto.Image")));
            this.Producto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Producto.Margin = new System.Windows.Forms.Padding(15, 1, 0, 2);
            this.Producto.Name = "Producto";
            this.Producto.Size = new System.Drawing.Size(61, 54);
            this.Producto.Text = "Productos";
            // 
            // Reporte
            // 
            this.Reporte.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Reporte.Image = ((System.Drawing.Image)(resources.GetObject("Reporte.Image")));
            this.Reporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Reporte.Margin = new System.Windows.Forms.Padding(15, 1, 0, 2);
            this.Reporte.Name = "Reporte";
            this.Reporte.Size = new System.Drawing.Size(61, 54);
            this.Reporte.Text = "Reportes";
            // 
            // Ayuda
            // 
            this.Ayuda.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Ayuda.Image = ((System.Drawing.Image)(resources.GetObject("Ayuda.Image")));
            this.Ayuda.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Ayuda.Margin = new System.Windows.Forms.Padding(650, 1, 0, 2);
            this.Ayuda.Name = "Ayuda";
            this.Ayuda.Size = new System.Drawing.Size(49, 54);
            this.Ayuda.Text = "Ayuda";
            // 
            // panel12
            // 
            this.panel12.AutoSize = true;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.txtNombre);
            this.panel12.Controls.Add(this.txtCedula);
            this.panel12.Controls.Add(this.lbCedula);
            this.panel12.Controls.Add(this.txtDireccion);
            this.panel12.Controls.Add(this.lbDireccion);
            this.panel12.Controls.Add(this.lbNombre);
            this.panel12.Controls.Add(this.txtTelefono);
            this.panel12.Controls.Add(this.lbTelefono);
            this.panel12.Controls.Add(this.lbApellido);
            this.panel12.Controls.Add(this.txtApellido);
            this.panel12.Location = new System.Drawing.Point(0, 94);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1030, 78);
            this.panel12.TabIndex = 4;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(205, 28);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 14;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // txtCedula
            // 
            this.txtCedula.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCedula.Location = new System.Drawing.Point(62, 28);
            this.txtCedula.MaxLength = 8;
            this.txtCedula.Name = "txtCedula";
            this.txtCedula.Size = new System.Drawing.Size(82, 20);
            this.txtCedula.TabIndex = 5;
            this.txtCedula.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCedula_KeyDown);
            this.txtCedula.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCedula_KeyPress);
            // 
            // lbCedula
            // 
            this.lbCedula.AutoSize = true;
            this.lbCedula.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCedula.Location = new System.Drawing.Point(7, 32);
            this.lbCedula.Name = "lbCedula";
            this.lbCedula.Size = new System.Drawing.Size(49, 13);
            this.lbCedula.TabIndex = 0;
            this.lbCedula.Text = "Cedula:";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(730, 28);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(295, 20);
            this.txtDireccion.TabIndex = 13;
            this.txtDireccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDireccion_KeyPress);
            // 
            // lbDireccion
            // 
            this.lbDireccion.AutoSize = true;
            this.lbDireccion.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDireccion.Location = new System.Drawing.Point(657, 32);
            this.lbDireccion.Name = "lbDireccion";
            this.lbDireccion.Size = new System.Drawing.Size(67, 13);
            this.lbDireccion.TabIndex = 12;
            this.lbDireccion.Text = "Dirección:";
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombre.Location = new System.Drawing.Point(150, 32);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(49, 13);
            this.lbNombre.TabIndex = 7;
            this.lbNombre.Text = "Nombre:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(551, 28);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(100, 20);
            this.txtTelefono.TabIndex = 10;
            this.txtTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono_KeyPress);
            // 
            // lbTelefono
            // 
            this.lbTelefono.AutoSize = true;
            this.lbTelefono.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTelefono.Location = new System.Drawing.Point(484, 32);
            this.lbTelefono.Name = "lbTelefono";
            this.lbTelefono.Size = new System.Drawing.Size(61, 13);
            this.lbTelefono.TabIndex = 11;
            this.lbTelefono.Text = "Teléfono:";
            // 
            // lbApellido
            // 
            this.lbApellido.AutoSize = true;
            this.lbApellido.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbApellido.Location = new System.Drawing.Point(311, 32);
            this.lbApellido.Name = "lbApellido";
            this.lbApellido.Size = new System.Drawing.Size(61, 13);
            this.lbApellido.TabIndex = 9;
            this.lbApellido.Text = "Apellido:";
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(378, 28);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(100, 20);
            this.txtApellido.TabIndex = 8;
            this.txtApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtApellido_KeyPress);
            // 
            // lbCliente
            // 
            this.lbCliente.AutoSize = true;
            this.lbCliente.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCliente.Location = new System.Drawing.Point(12, 79);
            this.lbCliente.Name = "lbCliente";
            this.lbCliente.Size = new System.Drawing.Size(80, 22);
            this.lbCliente.TabIndex = 15;
            this.lbCliente.Text = "Cliente";
            // 
            // txtNombreProducto
            // 
            this.txtNombreProducto.Location = new System.Drawing.Point(593, 62);
            this.txtNombreProducto.Name = "txtNombreProducto";
            this.txtNombreProducto.Size = new System.Drawing.Size(100, 20);
            this.txtNombreProducto.TabIndex = 19;
            // 
            // lbFactura
            // 
            this.lbFactura.AutoSize = true;
            this.lbFactura.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFactura.Location = new System.Drawing.Point(12, 175);
            this.lbFactura.Name = "lbFactura";
            this.lbFactura.Size = new System.Drawing.Size(80, 22);
            this.lbFactura.TabIndex = 32;
            this.lbFactura.Text = "Factura";
            // 
            // lbCodBarra
            // 
            this.lbCodBarra.AutoSize = true;
            this.lbCodBarra.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCodBarra.Location = new System.Drawing.Point(486, 26);
            this.lbCodBarra.Name = "lbCodBarra";
            this.lbCodBarra.Size = new System.Drawing.Size(103, 13);
            this.lbCodBarra.TabIndex = 16;
            this.lbCodBarra.Text = "Codigo de Barra:";
            // 
            // txtCodBarra
            // 
            this.txtCodBarra.Location = new System.Drawing.Point(594, 22);
            this.txtCodBarra.Name = "txtCodBarra";
            this.txtCodBarra.Size = new System.Drawing.Size(139, 20);
            this.txtCodBarra.TabIndex = 17;
            // 
            // lbNombreProducto
            // 
            this.lbNombreProducto.AutoSize = true;
            this.lbNombreProducto.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreProducto.Location = new System.Drawing.Point(540, 65);
            this.lbNombreProducto.Name = "lbNombreProducto";
            this.lbNombreProducto.Size = new System.Drawing.Size(49, 13);
            this.lbNombreProducto.TabIndex = 18;
            this.lbNombreProducto.Text = "Nombre:";
            // 
            // lbPrecio
            // 
            this.lbPrecio.AutoSize = true;
            this.lbPrecio.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPrecio.Location = new System.Drawing.Point(699, 65);
            this.lbPrecio.Name = "lbPrecio";
            this.lbPrecio.Size = new System.Drawing.Size(49, 13);
            this.lbPrecio.TabIndex = 20;
            this.lbPrecio.Text = "Precio:";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(745, 62);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(100, 20);
            this.txtPrecio.TabIndex = 21;
            // 
            // lbCantidad
            // 
            this.lbCantidad.AutoSize = true;
            this.lbCantidad.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCantidad.Location = new System.Drawing.Point(851, 66);
            this.lbCantidad.Name = "lbCantidad";
            this.lbCantidad.Size = new System.Drawing.Size(61, 13);
            this.lbCantidad.TabIndex = 22;
            this.lbCantidad.Text = "Cantidad:";
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(909, 62);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(39, 20);
            this.txtCantidad.TabIndex = 23;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.DataInformativo);
            this.panel3.Location = new System.Drawing.Point(15, 62);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(447, 213);
            this.panel3.TabIndex = 28;
            // 
            // DataInformativo
            // 
            this.DataInformativo.AllowUserToAddRows = false;
            this.DataInformativo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DataInformativo.BackgroundColor = System.Drawing.SystemColors.InactiveCaption;
            this.DataInformativo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataInformativo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataInformativo.Location = new System.Drawing.Point(-1, 2);
            this.DataInformativo.Name = "DataInformativo";
            this.DataInformativo.ReadOnly = true;
            this.DataInformativo.Size = new System.Drawing.Size(440, 206);
            this.DataInformativo.TabIndex = 29;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.Datafacturaventa);
            this.panel4.Location = new System.Drawing.Point(547, 88);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(444, 213);
            this.panel4.TabIndex = 29;
            // 
            // Datafacturaventa
            // 
            this.Datafacturaventa.BackgroundColor = System.Drawing.SystemColors.InactiveCaption;
            this.Datafacturaventa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Datafacturaventa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Datafacturaventa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nombre_Factura,
            this.Cantidad_Factura,
            this.Pvp_Factura,
            this.Total});
            this.Datafacturaventa.Location = new System.Drawing.Point(0, -1);
            this.Datafacturaventa.Name = "Datafacturaventa";
            this.Datafacturaventa.Size = new System.Drawing.Size(455, 197);
            this.Datafacturaventa.TabIndex = 30;
            // 
            // Nombre_Factura
            // 
            this.Nombre_Factura.HeaderText = "       Nombre";
            this.Nombre_Factura.Name = "Nombre_Factura";
            // 
            // Cantidad_Factura
            // 
            this.Cantidad_Factura.HeaderText = "      Cantidad";
            this.Cantidad_Factura.Name = "Cantidad_Factura";
            // 
            // Pvp_Factura
            // 
            this.Pvp_Factura.HeaderText = "       pvp";
            this.Pvp_Factura.Name = "Pvp_Factura";
            // 
            // Total
            // 
            this.Total.HeaderText = "        Total";
            this.Total.Name = "Total";
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoEllipsis = true;
            this.btnAgregar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnAgregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregar.Image")));
            this.btnAgregar.Location = new System.Drawing.Point(967, 51);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(36, 38);
            this.btnAgregar.TabIndex = 30;
            this.btnAgregar.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gainsboro;
            this.panel5.Controls.Add(this.btnCancelar);
            this.panel5.Controls.Add(this.btnFacturar);
            this.panel5.Controls.Add(this.lbCargaVuelto);
            this.panel5.Controls.Add(this.txtpago);
            this.panel5.Controls.Add(this.lbVuelto);
            this.panel5.Controls.Add(this.lbPago);
            this.panel5.Controls.Add(this.lbSubTotal);
            this.panel5.Controls.Add(this.lbTotalFactura);
            this.panel5.Location = new System.Drawing.Point(543, 310);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(468, 133);
            this.panel5.TabIndex = 31;
            // 
            // btnCancelar
            // 
            this.btnCancelar.AutoEllipsis = true;
            this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(357, 78);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(45, 53);
            this.btnCancelar.TabIndex = 36;
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnFacturar
            // 
            this.btnFacturar.AutoEllipsis = true;
            this.btnFacturar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnFacturar.FlatAppearance.BorderSize = 0;
            this.btnFacturar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnFacturar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFacturar.Image = ((System.Drawing.Image)(resources.GetObject("btnFacturar.Image")));
            this.btnFacturar.Location = new System.Drawing.Point(417, 84);
            this.btnFacturar.Name = "btnFacturar";
            this.btnFacturar.Size = new System.Drawing.Size(32, 33);
            this.btnFacturar.TabIndex = 34;
            this.btnFacturar.UseVisualStyleBackColor = true;
            this.btnFacturar.Click += new System.EventHandler(this.btnFacturar_Click);
            // 
            // lbCargaVuelto
            // 
            this.lbCargaVuelto.AutoSize = true;
            this.lbCargaVuelto.Location = new System.Drawing.Point(307, 56);
            this.lbCargaVuelto.Name = "lbCargaVuelto";
            this.lbCargaVuelto.Size = new System.Drawing.Size(35, 13);
            this.lbCargaVuelto.TabIndex = 34;
            this.lbCargaVuelto.Text = "label1";
            // 
            // txtpago
            // 
            this.txtpago.Location = new System.Drawing.Point(123, 53);
            this.txtpago.Name = "txtpago";
            this.txtpago.Size = new System.Drawing.Size(100, 20);
            this.txtpago.TabIndex = 32;
            // 
            // lbVuelto
            // 
            this.lbVuelto.AutoSize = true;
            this.lbVuelto.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVuelto.Location = new System.Drawing.Point(252, 55);
            this.lbVuelto.Name = "lbVuelto";
            this.lbVuelto.Size = new System.Drawing.Size(49, 15);
            this.lbVuelto.TabIndex = 33;
            this.lbVuelto.Text = "Vuelto";
            // 
            // lbPago
            // 
            this.lbPago.AutoSize = true;
            this.lbPago.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPago.Location = new System.Drawing.Point(82, 53);
            this.lbPago.Name = "lbPago";
            this.lbPago.Size = new System.Drawing.Size(35, 15);
            this.lbPago.TabIndex = 2;
            this.lbPago.Text = "Pago";
            // 
            // lbSubTotal
            // 
            this.lbSubTotal.AutoSize = true;
            this.lbSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSubTotal.Location = new System.Drawing.Point(324, 18);
            this.lbSubTotal.Name = "lbSubTotal";
            this.lbSubTotal.Size = new System.Drawing.Size(0, 31);
            this.lbSubTotal.TabIndex = 1;
            // 
            // lbTotalFactura
            // 
            this.lbTotalFactura.AutoSize = true;
            this.lbTotalFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalFactura.Location = new System.Drawing.Point(126, 9);
            this.lbTotalFactura.Name = "lbTotalFactura";
            this.lbTotalFactura.Size = new System.Drawing.Size(75, 31);
            this.lbTotalFactura.TabIndex = 0;
            this.lbTotalFactura.Text = "Total";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.label2);
            this.panel6.Controls.Add(this.dataGridView1);
            this.panel6.Controls.Add(this.txtBuscar);
            this.panel6.Controls.Add(this.btnBuscar);
            this.panel6.Controls.Add(this.btnAgregar);
            this.panel6.Controls.Add(this.panel5);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Controls.Add(this.txtCantidad);
            this.panel6.Controls.Add(this.lbCantidad);
            this.panel6.Controls.Add(this.txtPrecio);
            this.panel6.Controls.Add(this.lbPrecio);
            this.panel6.Controls.Add(this.txtNombreProducto);
            this.panel6.Controls.Add(this.lbNombreProducto);
            this.panel6.Controls.Add(this.txtCodBarra);
            this.panel6.Controls.Add(this.lbCodBarra);
            this.panel6.Location = new System.Drawing.Point(0, 194);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1026, 448);
            this.panel6.TabIndex = 32;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.codDataGridViewTextBoxColumn,
            this.nombreDataGridViewTextBoxColumn,
            this.cantidadDataGridViewTextBoxColumn,
            this.costoDataGridViewTextBoxColumn,
            this.pvpDataGridViewTextBoxColumn,
            this.estadoDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.productoBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(-1, 288);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(546, 136);
            this.dataGridView1.TabIndex = 34;
            // 
            // codDataGridViewTextBoxColumn
            // 
            this.codDataGridViewTextBoxColumn.DataPropertyName = "cod";
            this.codDataGridViewTextBoxColumn.HeaderText = "Código";
            this.codDataGridViewTextBoxColumn.Name = "codDataGridViewTextBoxColumn";
            this.codDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nombreDataGridViewTextBoxColumn
            // 
            this.nombreDataGridViewTextBoxColumn.DataPropertyName = "nombre";
            this.nombreDataGridViewTextBoxColumn.HeaderText = "Nombre";
            this.nombreDataGridViewTextBoxColumn.Name = "nombreDataGridViewTextBoxColumn";
            this.nombreDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cantidadDataGridViewTextBoxColumn
            // 
            this.cantidadDataGridViewTextBoxColumn.DataPropertyName = "cantidad";
            this.cantidadDataGridViewTextBoxColumn.HeaderText = "Cantidad";
            this.cantidadDataGridViewTextBoxColumn.Name = "cantidadDataGridViewTextBoxColumn";
            this.cantidadDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // costoDataGridViewTextBoxColumn
            // 
            this.costoDataGridViewTextBoxColumn.DataPropertyName = "costo";
            this.costoDataGridViewTextBoxColumn.HeaderText = "Costo";
            this.costoDataGridViewTextBoxColumn.Name = "costoDataGridViewTextBoxColumn";
            this.costoDataGridViewTextBoxColumn.ReadOnly = true;
            this.costoDataGridViewTextBoxColumn.Visible = false;
            // 
            // pvpDataGridViewTextBoxColumn
            // 
            this.pvpDataGridViewTextBoxColumn.DataPropertyName = "pvp";
            this.pvpDataGridViewTextBoxColumn.HeaderText = "Pvp";
            this.pvpDataGridViewTextBoxColumn.Name = "pvpDataGridViewTextBoxColumn";
            this.pvpDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // estadoDataGridViewTextBoxColumn
            // 
            this.estadoDataGridViewTextBoxColumn.DataPropertyName = "estado";
            this.estadoDataGridViewTextBoxColumn.HeaderText = "Estado";
            this.estadoDataGridViewTextBoxColumn.Name = "estadoDataGridViewTextBoxColumn";
            this.estadoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // productoBindingSource
            // 
            this.productoBindingSource.DataMember = "producto";
            this.productoBindingSource.DataSource = this.inventarioDataSet1;
            // 
            // inventarioDataSet1
            // 
            this.inventarioDataSet1.DataSetName = "InventarioDataSet1";
            this.inventarioDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(26, 26);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(237, 20);
            this.txtBuscar.TabIndex = 33;
            this.txtBuscar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscar_KeyPress);
            // 
            // btnBuscar
            // 
            this.btnBuscar.AutoEllipsis = true;
            this.btnBuscar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnBuscar.FlatAppearance.BorderSize = 0;
            this.btnBuscar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnBuscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(269, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(60, 53);
            this.btnBuscar.TabIndex = 32;
            this.btnBuscar.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // productoTableAdapter
            // 
            this.productoTableAdapter.ClearBeforeFill = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(265, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 22);
            this.label2.TabIndex = 35;
            this.label2.Text = "Lista de productos";
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(1030, 645);
            this.Controls.Add(this.lbFactura);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.lbCliente);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel1);
            this.Name = "frmPrincipal";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Home";
            this.TransparencyKey = System.Drawing.Color.Peru;
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataInformativo)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Datafacturaventa)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inventarioDataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel Home;
        private System.Windows.Forms.ToolStripSplitButton Producto;
        private System.Windows.Forms.ToolStripSplitButton Reporte;
        private System.Windows.Forms.ToolStripButton Ayuda;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label lbCliente;
        private System.Windows.Forms.Label lbCedula;
        private System.Windows.Forms.TextBox txtCedula;
       
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.Label lbApellido;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label lbTelefono;
        private System.Windows.Forms.Label lbDireccion;
        private System.Windows.Forms.TextBox txtDireccion;
       
        private System.Windows.Forms.Label lbCodBarra;
        private System.Windows.Forms.TextBox txtCodBarra;
        private System.Windows.Forms.Label lbNombreProducto;
        private System.Windows.Forms.TextBox txtNombreProducto;
        private System.Windows.Forms.Label lbPrecio;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label lbCantidad;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView DataInformativo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView Datafacturaventa;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lbTotalFactura;
        private System.Windows.Forms.Label lbSubTotal;
        private System.Windows.Forms.Label lbPago;
        private System.Windows.Forms.TextBox txtpago;
        private System.Windows.Forms.Label lbVuelto;
       
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lbFactura;
        private System.Windows.Forms.Label lbFecha;
        private System.Windows.Forms.ToolStripButton Proveedores;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lbCargaVuelto;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBuscar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pvp_Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnFacturar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private InventarioDataSet1 inventarioDataSet1;
        private System.Windows.Forms.BindingSource productoBindingSource;
        private InventarioDataSet1TableAdapters.productoTableAdapter productoTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn codDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn cantidadDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pvpDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn estadoDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label2;

    }
}


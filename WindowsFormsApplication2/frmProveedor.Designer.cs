﻿namespace WindowsFormsApplication2
{
    partial class frmProveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProveedor));
            this.lbProveedor = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtRif = new System.Windows.Forms.TextBox();
            this.lbRif = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.lbDireccion = new System.Windows.Forms.Label();
            this.lbNombre = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.lbTelefono = new System.Windows.Forms.Label();
            this.lbCorreo = new System.Windows.Forms.Label();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.Home = new System.Windows.Forms.ToolStripLabel();
            this.Proveedores = new System.Windows.Forms.ToolStripButton();
            this.Producto = new System.Windows.Forms.ToolStripSplitButton();
            this.Reporte = new System.Windows.Forms.ToolStripSplitButton();
            this.Ayuda = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbFecha = new System.Windows.Forms.Label();
            this.lbFactura = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.txtBuscarProd = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lbNumeroFactura = new System.Windows.Forms.Label();
            this.btnFacturar = new System.Windows.Forms.Button();
            this.lbCargaVuelto = new System.Windows.Forms.Label();
            this.txtpago = new System.Windows.Forms.TextBox();
            this.lbVuelto = new System.Windows.Forms.Label();
            this.lbPago = new System.Windows.Forms.Label();
            this.lbSubTotal = new System.Windows.Forms.Label();
            this.lbTotalFactura = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Datafacturaventa = new System.Windows.Forms.DataGridView();
            this.Nombre_Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad_Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pvp_Factura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.DataInformativo = new System.Windows.Forms.DataGridView();
            this.Cod_Barra = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre_Producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pvp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.lbCantidad = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.lbPrecio = new System.Windows.Forms.Label();
            this.txtNombreProducto = new System.Windows.Forms.TextBox();
            this.lbNombreProducto = new System.Windows.Forms.Label();
            this.txtCodBarra = new System.Windows.Forms.TextBox();
            this.lbCodBarra = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel12.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Datafacturaventa)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataInformativo)).BeginInit();
            this.SuspendLayout();
            // 
            // lbProveedor
            // 
            this.lbProveedor.AutoSize = true;
            this.lbProveedor.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProveedor.Location = new System.Drawing.Point(19, 77);
            this.lbProveedor.Name = "lbProveedor";
            this.lbProveedor.Size = new System.Drawing.Size(100, 22);
            this.lbProveedor.TabIndex = 36;
            this.lbProveedor.Text = "Proveedor";
            // 
            // panel12
            // 
            this.panel12.AutoSize = true;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.txtNombre);
            this.panel12.Controls.Add(this.txtRif);
            this.panel12.Controls.Add(this.lbRif);
            this.panel12.Controls.Add(this.txtDireccion);
            this.panel12.Controls.Add(this.lbDireccion);
            this.panel12.Controls.Add(this.lbNombre);
            this.panel12.Controls.Add(this.txtTelefono);
            this.panel12.Controls.Add(this.lbTelefono);
            this.panel12.Controls.Add(this.lbCorreo);
            this.panel12.Controls.Add(this.txtCorreo);
            this.panel12.Location = new System.Drawing.Point(7, 89);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(1034, 63);
            this.panel12.TabIndex = 35;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(172, 28);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 14;
            this.txtNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombre_KeyPress);
            // 
            // txtRif
            // 
            this.txtRif.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRif.Location = new System.Drawing.Point(29, 28);
            this.txtRif.MaxLength = 8;
            this.txtRif.Name = "txtRif";
            this.txtRif.Size = new System.Drawing.Size(82, 20);
            this.txtRif.TabIndex = 5;
            // 
            // lbRif
            // 
            this.lbRif.AutoSize = true;
            this.lbRif.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRif.Location = new System.Drawing.Point(3, 32);
            this.lbRif.Name = "lbRif";
            this.lbRif.Size = new System.Drawing.Size(31, 13);
            this.lbRif.TabIndex = 0;
            this.lbRif.Text = "Rif:";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(795, 28);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(234, 20);
            this.txtDireccion.TabIndex = 13;
            this.txtDireccion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDireccion_KeyPress);
            // 
            // lbDireccion
            // 
            this.lbDireccion.AutoSize = true;
            this.lbDireccion.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDireccion.Location = new System.Drawing.Point(730, 32);
            this.lbDireccion.Name = "lbDireccion";
            this.lbDireccion.Size = new System.Drawing.Size(67, 13);
            this.lbDireccion.TabIndex = 12;
            this.lbDireccion.Text = "Dirección:";
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombre.Location = new System.Drawing.Point(117, 32);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(49, 13);
            this.lbNombre.TabIndex = 7;
            this.lbNombre.Text = "Nombre:";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Location = new System.Drawing.Point(335, 28);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(100, 20);
            this.txtTelefono.TabIndex = 10;
            this.txtTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono_KeyPress);
            // 
            // lbTelefono
            // 
            this.lbTelefono.AutoSize = true;
            this.lbTelefono.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTelefono.Location = new System.Drawing.Point(278, 32);
            this.lbTelefono.Name = "lbTelefono";
            this.lbTelefono.Size = new System.Drawing.Size(61, 13);
            this.lbTelefono.TabIndex = 11;
            this.lbTelefono.Text = "Teléfono:";
            // 
            // lbCorreo
            // 
            this.lbCorreo.AutoSize = true;
            this.lbCorreo.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCorreo.Location = new System.Drawing.Point(441, 32);
            this.lbCorreo.Name = "lbCorreo";
            this.lbCorreo.Size = new System.Drawing.Size(49, 13);
            this.lbCorreo.TabIndex = 9;
            this.lbCorreo.Text = "Correo:";
            // 
            // txtCorreo
            // 
            this.txtCorreo.Location = new System.Drawing.Point(488, 28);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(236, 20);
            this.txtCorreo.TabIndex = 8;
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(45, 50);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Home,
            this.Proveedores,
            this.Producto,
            this.Reporte,
            this.Ayuda});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Margin = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1045, 57);
            this.toolStrip1.TabIndex = 34;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // Home
            // 
            this.Home.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Home.Image = ((System.Drawing.Image)(resources.GetObject("Home.Image")));
            this.Home.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(45, 54);
            this.Home.Text = "Home";
            this.Home.ToolTipText = "Home";
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // Proveedores
            // 
            this.Proveedores.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Proveedores.DoubleClickEnabled = true;
            this.Proveedores.Image = ((System.Drawing.Image)(resources.GetObject("Proveedores.Image")));
            this.Proveedores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Proveedores.Margin = new System.Windows.Forms.Padding(15, 1, 0, 2);
            this.Proveedores.Name = "Proveedores";
            this.Proveedores.Size = new System.Drawing.Size(49, 54);
            this.Proveedores.Text = "Proveedores";
            // 
            // Producto
            // 
            this.Producto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Producto.Image = ((System.Drawing.Image)(resources.GetObject("Producto.Image")));
            this.Producto.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Producto.Margin = new System.Windows.Forms.Padding(15, 1, 0, 2);
            this.Producto.Name = "Producto";
            this.Producto.Size = new System.Drawing.Size(61, 54);
            this.Producto.Text = "Productos";
            // 
            // Reporte
            // 
            this.Reporte.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Reporte.Image = ((System.Drawing.Image)(resources.GetObject("Reporte.Image")));
            this.Reporte.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Reporte.Margin = new System.Windows.Forms.Padding(15, 1, 0, 2);
            this.Reporte.Name = "Reporte";
            this.Reporte.Size = new System.Drawing.Size(61, 54);
            this.Reporte.Text = "Reportes";
            // 
            // Ayuda
            // 
            this.Ayuda.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Ayuda.Image = ((System.Drawing.Image)(resources.GetObject("Ayuda.Image")));
            this.Ayuda.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Ayuda.Margin = new System.Windows.Forms.Padding(650, 1, 0, 2);
            this.Ayuda.Name = "Ayuda";
            this.Ayuda.Size = new System.Drawing.Size(49, 54);
            this.Ayuda.Text = "Ayuda";
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.lbFecha);
            this.panel1.Location = new System.Drawing.Point(7, 54);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1030, 16);
            this.panel1.TabIndex = 33;
            // 
            // lbFecha
            // 
            this.lbFecha.AutoSize = true;
            this.lbFecha.Location = new System.Drawing.Point(475, 3);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(40, 13);
            this.lbFecha.TabIndex = 0;
            this.lbFecha.Text = "Fecha:";
            // 
            // lbFactura
            // 
            this.lbFactura.AutoSize = true;
            this.lbFactura.Font = new System.Drawing.Font("Consolas", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFactura.Location = new System.Drawing.Point(12, 163);
            this.lbFactura.Name = "lbFactura";
            this.lbFactura.Size = new System.Drawing.Size(80, 22);
            this.lbFactura.TabIndex = 37;
            this.lbFactura.Text = "Factura";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.txtBuscarProd);
            this.panel6.Controls.Add(this.btnBuscar);
            this.panel6.Controls.Add(this.btnAgregar);
            this.panel6.Controls.Add(this.panel5);
            this.panel6.Controls.Add(this.panel4);
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Controls.Add(this.txtCantidad);
            this.panel6.Controls.Add(this.lbCantidad);
            this.panel6.Controls.Add(this.txtPrecio);
            this.panel6.Controls.Add(this.lbPrecio);
            this.panel6.Controls.Add(this.txtNombreProducto);
            this.panel6.Controls.Add(this.lbNombreProducto);
            this.panel6.Controls.Add(this.txtCodBarra);
            this.panel6.Controls.Add(this.lbCodBarra);
            this.panel6.Location = new System.Drawing.Point(0, 182);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(1037, 448);
            this.panel6.TabIndex = 38;
            // 
            // txtBuscarProd
            // 
            this.txtBuscarProd.Location = new System.Drawing.Point(26, 26);
            this.txtBuscarProd.Name = "txtBuscarProd";
            this.txtBuscarProd.Size = new System.Drawing.Size(237, 20);
            this.txtBuscarProd.TabIndex = 33;
            this.txtBuscarProd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBuscarProd_KeyPress);
            // 
            // btnBuscar
            // 
            this.btnBuscar.AutoEllipsis = true;
            this.btnBuscar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnBuscar.FlatAppearance.BorderSize = 0;
            this.btnBuscar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnBuscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(269, 14);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(51, 42);
            this.btnBuscar.TabIndex = 32;
            this.btnBuscar.UseVisualStyleBackColor = true;
            // 
            // btnAgregar
            // 
            this.btnAgregar.AutoEllipsis = true;
            this.btnAgregar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnAgregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregar.Image")));
            this.btnAgregar.Location = new System.Drawing.Point(967, 51);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(36, 38);
            this.btnAgregar.TabIndex = 30;
            this.btnAgregar.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gainsboro;
            this.panel5.Controls.Add(this.textBox3);
            this.panel5.Controls.Add(this.btnCancelar);
            this.panel5.Controls.Add(this.lbNumeroFactura);
            this.panel5.Controls.Add(this.btnFacturar);
            this.panel5.Controls.Add(this.lbCargaVuelto);
            this.panel5.Controls.Add(this.txtpago);
            this.panel5.Controls.Add(this.lbVuelto);
            this.panel5.Controls.Add(this.lbPago);
            this.panel5.Controls.Add(this.lbSubTotal);
            this.panel5.Controls.Add(this.lbTotalFactura);
            this.panel5.Location = new System.Drawing.Point(523, 307);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(468, 133);
            this.panel5.TabIndex = 31;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(246, 92);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 35;
            // 
            // btnCancelar
            // 
            this.btnCancelar.AutoEllipsis = true;
            this.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnCancelar.FlatAppearance.BorderSize = 0;
            this.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(357, 78);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(45, 53);
            this.btnCancelar.TabIndex = 36;
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // lbNumeroFactura
            // 
            this.lbNumeroFactura.AutoSize = true;
            this.lbNumeroFactura.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNumeroFactura.Location = new System.Drawing.Point(125, 95);
            this.lbNumeroFactura.Name = "lbNumeroFactura";
            this.lbNumeroFactura.Size = new System.Drawing.Size(115, 13);
            this.lbNumeroFactura.TabIndex = 34;
            this.lbNumeroFactura.Text = "Numero de Factura:";
            // 
            // btnFacturar
            // 
            this.btnFacturar.AutoEllipsis = true;
            this.btnFacturar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnFacturar.FlatAppearance.BorderSize = 0;
            this.btnFacturar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Black;
            this.btnFacturar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnFacturar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFacturar.Image = ((System.Drawing.Image)(resources.GetObject("btnFacturar.Image")));
            this.btnFacturar.Location = new System.Drawing.Point(417, 84);
            this.btnFacturar.Name = "btnFacturar";
            this.btnFacturar.Size = new System.Drawing.Size(32, 33);
            this.btnFacturar.TabIndex = 34;
            this.btnFacturar.UseVisualStyleBackColor = true;
            // 
            // lbCargaVuelto
            // 
            this.lbCargaVuelto.AutoSize = true;
            this.lbCargaVuelto.Location = new System.Drawing.Point(307, 56);
            this.lbCargaVuelto.Name = "lbCargaVuelto";
            this.lbCargaVuelto.Size = new System.Drawing.Size(35, 13);
            this.lbCargaVuelto.TabIndex = 34;
            this.lbCargaVuelto.Text = "label1";
            // 
            // txtpago
            // 
            this.txtpago.Location = new System.Drawing.Point(123, 53);
            this.txtpago.Name = "txtpago";
            this.txtpago.Size = new System.Drawing.Size(100, 20);
            this.txtpago.TabIndex = 32;
            // 
            // lbVuelto
            // 
            this.lbVuelto.AutoSize = true;
            this.lbVuelto.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVuelto.Location = new System.Drawing.Point(252, 55);
            this.lbVuelto.Name = "lbVuelto";
            this.lbVuelto.Size = new System.Drawing.Size(49, 15);
            this.lbVuelto.TabIndex = 33;
            this.lbVuelto.Text = "Vuelto";
            // 
            // lbPago
            // 
            this.lbPago.AutoSize = true;
            this.lbPago.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPago.Location = new System.Drawing.Point(82, 53);
            this.lbPago.Name = "lbPago";
            this.lbPago.Size = new System.Drawing.Size(35, 15);
            this.lbPago.TabIndex = 2;
            this.lbPago.Text = "Pago";
            // 
            // lbSubTotal
            // 
            this.lbSubTotal.AutoSize = true;
            this.lbSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSubTotal.Location = new System.Drawing.Point(324, 18);
            this.lbSubTotal.Name = "lbSubTotal";
            this.lbSubTotal.Size = new System.Drawing.Size(0, 31);
            this.lbSubTotal.TabIndex = 1;
            // 
            // lbTotalFactura
            // 
            this.lbTotalFactura.AutoSize = true;
            this.lbTotalFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalFactura.Location = new System.Drawing.Point(126, 9);
            this.lbTotalFactura.Name = "lbTotalFactura";
            this.lbTotalFactura.Size = new System.Drawing.Size(75, 31);
            this.lbTotalFactura.TabIndex = 0;
            this.lbTotalFactura.Text = "Total";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.Datafacturaventa);
            this.panel4.Location = new System.Drawing.Point(547, 88);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(444, 213);
            this.panel4.TabIndex = 29;
            // 
            // Datafacturaventa
            // 
            this.Datafacturaventa.BackgroundColor = System.Drawing.SystemColors.InactiveCaption;
            this.Datafacturaventa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Datafacturaventa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Datafacturaventa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nombre_Factura,
            this.Cantidad_Factura,
            this.Pvp_Factura,
            this.Total});
            this.Datafacturaventa.Location = new System.Drawing.Point(0, -1);
            this.Datafacturaventa.Name = "Datafacturaventa";
            this.Datafacturaventa.Size = new System.Drawing.Size(443, 197);
            this.Datafacturaventa.TabIndex = 30;
            // 
            // Nombre_Factura
            // 
            this.Nombre_Factura.HeaderText = "       Nombre";
            this.Nombre_Factura.Name = "Nombre_Factura";
            // 
            // Cantidad_Factura
            // 
            this.Cantidad_Factura.HeaderText = "      Cantidad";
            this.Cantidad_Factura.Name = "Cantidad_Factura";
            // 
            // Pvp_Factura
            // 
            this.Pvp_Factura.HeaderText = "       pvp";
            this.Pvp_Factura.Name = "Pvp_Factura";
            // 
            // Total
            // 
            this.Total.HeaderText = "        Total";
            this.Total.Name = "Total";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.DataInformativo);
            this.panel3.Location = new System.Drawing.Point(11, 92);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(418, 213);
            this.panel3.TabIndex = 28;
            // 
            // DataInformativo
            // 
            this.DataInformativo.BackgroundColor = System.Drawing.SystemColors.InactiveCaption;
            this.DataInformativo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DataInformativo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataInformativo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Cod_Barra,
            this.Nombre_Producto,
            this.Cantidad,
            this.Pvp});
            this.DataInformativo.Location = new System.Drawing.Point(-9, -1);
            this.DataInformativo.Name = "DataInformativo";
            this.DataInformativo.ReadOnly = true;
            this.DataInformativo.Size = new System.Drawing.Size(429, 193);
            this.DataInformativo.TabIndex = 29;
            // 
            // Cod_Barra
            // 
            this.Cod_Barra.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Cod_Barra.FillWeight = 200F;
            this.Cod_Barra.HeaderText = "       Código";
            this.Cod_Barra.Name = "Cod_Barra";
            this.Cod_Barra.ReadOnly = true;
            this.Cod_Barra.Width = 86;
            // 
            // Nombre_Producto
            // 
            this.Nombre_Producto.HeaderText = "         Nombre";
            this.Nombre_Producto.MinimumWidth = 10;
            this.Nombre_Producto.Name = "Nombre_Producto";
            this.Nombre_Producto.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "        Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // Pvp
            // 
            this.Pvp.HeaderText = "         pvp";
            this.Pvp.Name = "Pvp";
            this.Pvp.ReadOnly = true;
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(909, 62);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(39, 20);
            this.txtCantidad.TabIndex = 23;
            this.txtCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCantidad_KeyPress);
            // 
            // lbCantidad
            // 
            this.lbCantidad.AutoSize = true;
            this.lbCantidad.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCantidad.Location = new System.Drawing.Point(851, 66);
            this.lbCantidad.Name = "lbCantidad";
            this.lbCantidad.Size = new System.Drawing.Size(61, 13);
            this.lbCantidad.TabIndex = 22;
            this.lbCantidad.Text = "Cantidad:";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(745, 62);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(100, 20);
            this.txtPrecio.TabIndex = 21;
            this.txtPrecio.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrecio_KeyPress);
            // 
            // lbPrecio
            // 
            this.lbPrecio.AutoSize = true;
            this.lbPrecio.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPrecio.Location = new System.Drawing.Point(699, 65);
            this.lbPrecio.Name = "lbPrecio";
            this.lbPrecio.Size = new System.Drawing.Size(49, 13);
            this.lbPrecio.TabIndex = 20;
            this.lbPrecio.Text = "Precio:";
            // 
            // txtNombreProducto
            // 
            this.txtNombreProducto.Location = new System.Drawing.Point(593, 62);
            this.txtNombreProducto.Name = "txtNombreProducto";
            this.txtNombreProducto.Size = new System.Drawing.Size(100, 20);
            this.txtNombreProducto.TabIndex = 19;
            this.txtNombreProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNombreProducto_KeyPress);
            // 
            // lbNombreProducto
            // 
            this.lbNombreProducto.AutoSize = true;
            this.lbNombreProducto.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreProducto.Location = new System.Drawing.Point(540, 65);
            this.lbNombreProducto.Name = "lbNombreProducto";
            this.lbNombreProducto.Size = new System.Drawing.Size(49, 13);
            this.lbNombreProducto.TabIndex = 18;
            this.lbNombreProducto.Text = "Nombre:";
            // 
            // txtCodBarra
            // 
            this.txtCodBarra.Location = new System.Drawing.Point(594, 22);
            this.txtCodBarra.Name = "txtCodBarra";
            this.txtCodBarra.Size = new System.Drawing.Size(139, 20);
            this.txtCodBarra.TabIndex = 17;
            // 
            // lbCodBarra
            // 
            this.lbCodBarra.AutoSize = true;
            this.lbCodBarra.Font = new System.Drawing.Font("Consolas", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCodBarra.Location = new System.Drawing.Point(486, 26);
            this.lbCodBarra.Name = "lbCodBarra";
            this.lbCodBarra.Size = new System.Drawing.Size(103, 13);
            this.lbCodBarra.TabIndex = 16;
            this.lbCodBarra.Text = "Codigo de Barra:";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmProveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.ClientSize = new System.Drawing.Size(1045, 659);
            this.Controls.Add(this.lbFactura);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.lbProveedor);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel1);
            this.Name = "frmProveedor";
            this.Text = "frmProveedorcs";
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Datafacturaventa)).EndInit();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataInformativo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbProveedor;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtRif;
        private System.Windows.Forms.Label lbRif;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label lbDireccion;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label lbTelefono;
        private System.Windows.Forms.Label lbCorreo;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel Home;
        private System.Windows.Forms.ToolStripButton Proveedores;
        private System.Windows.Forms.ToolStripSplitButton Producto;
        private System.Windows.Forms.ToolStripSplitButton Reporte;
        private System.Windows.Forms.ToolStripButton Ayuda;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbFecha;
        private System.Windows.Forms.Label lbFactura;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox txtBuscarProd;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lbNumeroFactura;
        private System.Windows.Forms.Button btnFacturar;
        private System.Windows.Forms.Label lbCargaVuelto;
        private System.Windows.Forms.TextBox txtpago;
        private System.Windows.Forms.Label lbVuelto;
        private System.Windows.Forms.Label lbPago;
        private System.Windows.Forms.Label lbSubTotal;
        private System.Windows.Forms.Label lbTotalFactura;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView Datafacturaventa;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad_Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pvp_Factura;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView DataInformativo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cod_Barra;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre_Producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pvp;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.Label lbCantidad;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label lbPrecio;
        private System.Windows.Forms.TextBox txtNombreProducto;
        private System.Windows.Forms.Label lbNombreProducto;
        private System.Windows.Forms.TextBox txtCodBarra;
        private System.Windows.Forms.Label lbCodBarra;
        private System.Windows.Forms.Timer timer1;
    }
}
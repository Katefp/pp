﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Modelos;

namespace WindowsFormsApplication2
{
    public partial class frmPrincipal : Form
    {
        cliente Cliente = new cliente();
        Producto pp = new Producto();
        
        cValidar val = new cValidar();
        public frmPrincipal()
        {
            InitializeComponent();
            timer1.Enabled = true;
        }


        private void timer1_Tick_1(object sender, EventArgs e)
        {
            lbFecha.Text = DateTime.Now.ToString();
        }
       

        private void txtCedula_KeyPress(object sender, KeyPressEventArgs v)
        {
            val.SoloNumeros(v);
        }

        private void txtNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            val.SoloLetras(e);
        }

        private void txtApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
         
            val.SoloLetras(e);
        }

        private void txtDireccion_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            val.SoloLetras(e);
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            
            val.SoloNumeros(e);
        }

        /// <summary>
        ///evento de búsqueda
        private void txtCedula_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) {
                
                Cliente = Cliente.BuscarCliente(txtCedula.Text);
                

                if (Cliente.cedula != null )
                {
                    txtCedula.Text = Cliente.cedula;
                    txtNombre.Text = Cliente.nombre;
                    txtApellido.Text = Cliente.apellido;
                    txtDireccion.Text = Cliente.direccion;
                    txtTelefono.Text = Cliente.telefono;
                }
                else
                {
                    MessageBox.Show("CLIENTE NO REGISTRADO", "Búsqueda", MessageBoxButtons.OK, MessageBoxIcon.Information);
                 }
                
            }
         
        }

        private void Proveedores_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmProveedor frm = new frmProveedor();
            frm.Show();

        }

        public bool validarCliente() {
            if (txtNombre.Text == Cliente.nombre && txtApellido.Text == Cliente.apellido && txtCedula.Text == Cliente.cedula && txtDireccion.Text == Cliente.direccion && txtTelefono.Text == Cliente.telefono) {
                return true;
            }else{
                return false;
            }
        }

        private void btnFacturar_Click(object sender, EventArgs e)
        {
            if(!validarCliente())
            {
                MessageBox.Show("¿Desea Cambiar los datos del cliente?", "Actualizacion", MessageBoxButtons.OK, MessageBoxIcon.Question);
            }
            if (validarCliente() == true) {
            ///Sentencias de trans 

            }

            //MessageBox.Show("Inserto con éxito");
            //cLimpiar limpiar = new cLimpiar();
            //limpiar.BorrarCampos(this);
        }

       


        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            //List<Producto> ListaProducto = new List<Producto>();
            //Producto producto = new Producto();
            pp.Nombre = e.KeyChar.ToString();
           
            //dataGridView1.DataSource = pp.buscarProducto(pp);
            //dataGridView1.DataMember = "consulta";

             DataInformativo.DataSource = pp.buscarProducto(pp);
             DataInformativo.DataMember = "productos";
            ////DataInformativo.Rows.Add(pp.buscarProducto(pp));
            ////DataInformativo.DataSource = pp.buscarProducto(pp);
            //ListaProducto = pp.buscarProducto(pp);
           
           
           
            

            //foreach (Producto s in ListaProducto)
            //{
            //    DataInformativo.Rows.Add(s);
            //    DataInformativo.Rows.Add(s);
            //    DataInformativo.Rows.Add(s);
            //    DataInformativo.Rows.Add(s);
            //}

            //DataInformativo.DataMember = "consulta";
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'inventarioDataSet1.producto' table. You can move, or remove it, as needed.
            this.productoTableAdapter.Fill(this.inventarioDataSet1.producto);

        }

       
        
    }
}
